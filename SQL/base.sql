-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table cmsedu.campuses
CREATE TABLE IF NOT EXISTS `campuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_campus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.campuses: ~2 rows (approximately)
DELETE FROM `campuses`;
/*!40000 ALTER TABLE `campuses` DISABLE KEYS */;
INSERT INTO `campuses` (`id`, `name_campus`, `created_at`, `updated_at`) VALUES
	(21, 'Bogota', '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(22, 'Fusagasuga', '2021-01-20 01:59:22', '2021-01-20 01:59:22');
/*!40000 ALTER TABLE `campuses` ENABLE KEYS */;

-- Dumping structure for table cmsedu.faculties
CREATE TABLE IF NOT EXISTS `faculties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_faculty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campus_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.faculties: ~2 rows (approximately)
DELETE FROM `faculties`;
/*!40000 ALTER TABLE `faculties` DISABLE KEYS */;
INSERT INTO `faculties` (`id`, `name_faculty`, `campus_id`, `created_at`, `updated_at`) VALUES
	(161, 'Ingenieria', '22', '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(162, 'Licenciatura', '21', '2021-01-20 01:59:22', '2021-01-20 01:59:22');
/*!40000 ALTER TABLE `faculties` ENABLE KEYS */;

-- Dumping structure for table cmsedu.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table cmsedu.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.migrations: ~7 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2021_01_18_204713_create_programas_table', 1),
	(5, '2021_01_18_211155_create_faculties_table', 1),
	(6, '2021_01_18_223751_create_campuses_table', 1),
	(7, '2021_01_19_221947_create_programa_user_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table cmsedu.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table cmsedu.programas
CREATE TABLE IF NOT EXISTS `programas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlimg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faculty_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.programas: ~4 rows (approximately)
DELETE FROM `programas`;
/*!40000 ALTER TABLE `programas` DISABLE KEYS */;
INSERT INTO `programas` (`id`, `name_program`, `urlimg`, `faculty_id`, `created_at`, `updated_at`) VALUES
	(16, 'Sistemas', '4a85a40fd8e991a0341f7e9edacd2e63.webp', 161, '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(17, 'Electronica', 'ab2d267b2a745b798b5ad7afc60bb8a4.webp', 161, '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(18, 'Sociales', '6841cec9c90b785991125b00d465feaf.webp', 161, '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(19, 'Matematicas', 'maths_banner_4x.webp', 161, '2021-01-20 01:59:22', '2021-01-20 01:59:22');
/*!40000 ALTER TABLE `programas` ENABLE KEYS */;

-- Dumping structure for table cmsedu.programa_user
CREATE TABLE IF NOT EXISTS `programa_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `programa_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.programa_user: ~0 rows (approximately)
DELETE FROM `programa_user`;
/*!40000 ALTER TABLE `programa_user` DISABLE KEYS */;
INSERT INTO `programa_user` (`id`, `user_id`, `programa_id`, `created_at`, `updated_at`) VALUES
	(1, 5, 16, NULL, NULL),
	(3, 1, 16, NULL, NULL),
	(4, 5, 17, NULL, NULL);
/*!40000 ALTER TABLE `programa_user` ENABLE KEYS */;

-- Dumping structure for table cmsedu.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Priname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Secname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nacionality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Cresidential` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Corigin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table cmsedu.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `Priname`, `Secname`, `Nacionality`, `Address`, `Phone`, `Cresidential`, `Corigin`, `created_at`, `updated_at`) VALUES
	(1, 'Osman', 'osmanjimenez@gmail.com', NULL, '$2y$10$HRTs84gVT84xzmWk3YJYYOf9m37rSZ80c62dU1CTxjqBmcSSJg0QS', NULL, 'Jimenez', 'Cortes', 'Colombia', 'Calle 19', '32220505', 'Fusagasuga', 'Fusagasuga', '2021-01-20 01:59:22', '2021-01-20 01:59:22'),
	(5, 'Armando', 'osmanjimenez98@gmail.com', NULL, '$2y$10$IvHeCzJYRSLCghI7nI2n9.zWu65MLueDSQ83v5nxXwo0MOtFudswi', NULL, 'Jimenez', 'Cortes', 'Colombia', 'Calle 19', '32220505', 'Fusagasuga', 'Fusagasuga', '2021-01-20 01:59:22', '2021-01-20 01:59:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
