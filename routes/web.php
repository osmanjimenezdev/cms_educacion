<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@home');
Route::get('programas', 'PagesController@programas');
Route::get('facultades', 'PagesController@facultades');

Route::group([
    'prefix' => 'admin', 
    'namespace' => 'Admin', 
    'middleware' => 'auth'], 
function(){
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('campus', 'CampusController@index')->name('admin.campus.index');
    Route::get('campus/create', 'CampusController@create')->name('admin.campus.create');
    Route::post('campus', 'CampusController@store' )->name('admin.campus.store');

    
    //Rutas de Administración
});


Route::group([
    'prefix' => 'admin', 
    'namespace' => 'Admin', 
    'middleware' => 'auth'], 
function(){
    Route::get('facultad', 'FacultyController@index')->name('admin.facultad.index');
    Route::get('facultad/create', 'FacultyController@create')->name('admin.facultad.create');
    Route::post('facultad', 'FacultyController@store' )->name('admin.facultad.store');

    
    //Rutas de Administración
});

Route::group([
    'prefix' => 'admin', 
    'namespace' => 'Admin', 
    'middleware' => 'auth'], 
function(){
    Route::get('programa', 'ProgramaController@index')->name('admin.programa.index');
    Route::get('programa/create', 'ProgramaController@create')->name('admin.programa.create');
    Route::put('programa', 'ProgramaController@store' )->name('admin.programa.store');

    
    //Rutas de Administración
});

Route::group([
    'prefix' => 'admin', 
    'namespace' => 'Admin', 
    'middleware' => 'auth'], 
function(){
    Route::get('estudiante', 'UserController@index')->name('admin.user.index');
    Route::get('estudiante/create', 'UserController@create')->name('admin.user.create');
    Route::post('estudiante', 'UserController@store' )->name('admin.user.store');

    
    //Rutas de Administración
});



Route::auth();

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
