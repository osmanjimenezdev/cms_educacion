<?php

namespace App\Http\Controllers\Admin;

use App\Campus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CampusController extends Controller
{
    public function index()
    {
        $campus = Campus::all();

        return view('admin.campus.index', compact('campus'));
    }

    public function create()
    {
        $campus = Campus::all();

        return view('admin.campus.create', compact('campus'));
    }

    public function store(Request $request)
    {
        $campus = new Campus;
        $campus->id =$request->get('Code');
        $campus->name_campus =$request->get('Name');
        $campus->save();

        return redirect()->route('admin.campus.index');

    }


}
