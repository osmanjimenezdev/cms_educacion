<?php

namespace App\Http\Controllers\Admin;

use App\Programa;
use App\Faculty;
use App\programa_user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgramaController extends Controller
{
    public function index()
    {
        $programas = Programa::all();

        return view('admin.programa.index', compact('programas'));
    }

    public function create()
    {
        $programas = Programa::all();
        $facultades = Faculty::all();

        return view('admin.programa.create', compact('programas', 'facultades'));
    }

    public function store(Request $request)
    {
        $programas = new Programa;

        if ($request->hasFile('urlimg')) {
            $file = $request->file('urlimg');
            $img = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/images/', $img);
        }


        $programas->id =$request->get('Code');
        $programas->name_program =$request->get('Name');
        $programas->faculty_id =$request->get('Facultad');
        $programas->urlimg = $img;
        $programas->save(); 

        return redirect()->route('admin.programa.index');
    }


}
