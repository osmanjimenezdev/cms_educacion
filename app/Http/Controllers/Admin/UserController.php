<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Programa;
use App\Faculty;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Post;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.user.index', compact('users'));

    }

    public function create()
    {
        $users = User::all();
        $programas = Programa::all();

        return view('admin.user.create', compact('users', 'programas'));
    }


    public function store(Request $request)
    {
       /* return $request->all();*/


        $User = new User;
        $User->id =$request->get('id');
        $User->name =$request->get('Name');
        $User->email =$request->get('Email');
        $User->password =Hash::make($request['Password']);

        $User->Priname =$request->get('Priname');
        $User->Secname =$request->get('Secname');
        $User->Nacionality =$request->get('Nacionality');
        $User->Address =$request->get('Address');
        $User->Phone =$request->get('Phone');
        $User->Cresidential =$request->get('Cresidential');
        $User->Corigin =$request->get('Corigin');

        $User->save();

        $User->programa()->attach($request->get('codes'));


        return redirect()->route('admin.user.index');



        

    }
}
