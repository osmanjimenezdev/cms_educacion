<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        /*=====================================================||
        || Graph variables on the number of comments per month ||
        ||=====================================================*/

        $Sistemas = DB::select('SELECT count(*) AS cantidad FROM programa_user WHERE programa_id = 16 ');
        $Electronica = DB::select('SELECT count(*) AS cantidad FROM programa_user WHERE programa_id = 17 ');
        $Sociales = DB::select('SELECT count(*) AS cantidad FROM programa_user WHERE programa_id = 18 ');
        $Matematicas = DB::select('SELECT count(*) AS cantidad FROM programa_user WHERE programa_id = 19 ');
       
    
        

        return view('admin.dashboard', compact(
            'Sistemas',
            'Electronica',
            'Sociales',
            'Matematicas'
        ));
    }

}