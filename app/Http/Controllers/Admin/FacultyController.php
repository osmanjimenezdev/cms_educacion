<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Campus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function index()
    {
        $facultades = Faculty::all();

        return view('admin.facultad.index', compact('facultades'));
    }

    public function create()
    {
        $facultades = Faculty::all();
        $campus = Campus::all();

        return view('admin.facultad.create', compact('facultades', 'campus'));
    }

    public function store(Request $request)
    {
        $facultades = new Faculty;
        $facultades->id =$request->get('Code');
        $facultades->name_faculty =$request->get('Name');
        $facultades->campus_id =$request->get('Code');
        $facultades->save();

        return redirect()->route('admin.facultad.index');

    }


}
