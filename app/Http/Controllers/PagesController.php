<?php

namespace App\Http\Controllers;

use App\Programa;
use App\Faculty;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(){
        $programas = Programa::latest('created_at')->get();
        return view('welcome', compact('programas'));
    }

    public function programas(){
        return Programa::all();
    }

    public function facultades(){
        return Faculty::all();
    }



}
