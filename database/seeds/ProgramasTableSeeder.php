<?php

use App\Programa;
use App\Faculty;
use App\Campus;
use App\User;
use App\programa_user;

use Illuminate\Database\Seeder;

class ProgramasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Programa::truncate();
        Faculty::truncate();
        Campus::truncate();
        User::truncate();

        /* ====== CAMPUS ====== */
        $campus = new Campus;
        $campus->id = "22";
        $campus->name_campus = "Fusagasuga";
        $campus->save();

        $campus = new Campus;
        $campus->id = "21";
        $campus->name_campus = "Bogota";
        $campus->save();

        /* ====== FACULTAD ====== */

        $faculty = new Faculty;
        $faculty->id = "161";
        $faculty->name_faculty = "Ingenieria";
        $faculty->campus_id = "22";
        $faculty->save();

        $faculty = new Faculty;
        $faculty->id = "162";
        $faculty->name_faculty = "Licenciatura";
        $faculty->campus_id = "21";
        $faculty->save();

        /* ====== PROGRAMA ====== */

        $programa = new Programa;
        $programa->id = "16";
        $programa->urlimg = "4a85a40fd8e991a0341f7e9edacd2e63.webp";
        $programa->name_program = "Sistemas";
        $programa->faculty_id = "161";
        $programa->save();

        $programa = new Programa;
        $programa->id = "17";
        $programa->urlimg = "ab2d267b2a745b798b5ad7afc60bb8a4.webp";
        $programa->name_program = "Electronica";
        $programa->faculty_id = "161";
        $programa->save();

        $programa = new Programa;
        $programa->id = "18";
        $programa->urlimg = "6841cec9c90b785991125b00d465feaf.webp";
        $programa->name_program = "Sociales";
        $programa->faculty_id = "161";
        $programa->save();

        $programa = new Programa;
        $programa->id = "19";
        $programa->urlimg = "maths_banner_4x.webp";
        $programa->name_program = "Matematicas";
        $programa->faculty_id = "161";
        $programa->save();

        /* ====== USUARIOS ====== */

        $user = new User;
        $user->id = "1";
        $user->name = "Osman";
        $user->email = "osmanjimenez@gmail.com";
        $user->password = Hash::make('123456789');
        $user->Priname = "Jimenez";
        $user->Secname = "Cortes";
        $user->Nacionality = "Colombia";
        $user->Address = "Calle 19";
        $user->Phone = "32220505";
        $user->Cresidential = "Fusagasuga";
        $user->Corigin = "Fusagasuga";
        $user->save();

        $user = new User;
        $user->id = "2";
        $user->name = "Armando";
        $user->email = "osmanjimenez98@gmail.com";
        $user->password = Hash::make('123456789');
        $user->Priname = "Jimenez";
        $user->Secname = "Cortes";
        $user->Nacionality = "Colombia";
        $user->Address = "Calle 19";
        $user->Phone = "32220505";
        $user->Cresidential = "Fusagasuga";
        $user->Corigin = "Fusagasuga";
        $user->save();

    }
}
