@extends('admin.layout')

@section('content')
    <div class="row ml-2 mr-2">
    <div class="col-12">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
                google.charts.load('current', {
                    'packages': ['bar']
                });
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Mes', 'Cantidad'],
                        ['Sistemas', {{ $Sistemas[0]->cantidad }} ],
                        ['Electronica', {{ $Electronica[0]->cantidad }}],
                        ['Sociales', {{ $Sociales[0]->cantidad }}],
                        ['Matematicas', {{ $Matematicas[0]->cantidad }}]

                    ]);

                    var options = {
                        bars: 'vertical' // Required for Material Bar Charts.
                    };

                    var chart = new google.charts.Bar(document.getElementById('barchart_material'));

                    chart.draw(data, google.charts.Bar.convertOptions(options));
                }
            </script>

            <div class="card shadow p-3 mb-5 bg-white rounded border-top border-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="far fa-chart-bar"></i>
                        Cantidad de Estudiantes por Programa
                    </h3>
                </div>
                <div class="card-body">
                    <div id="barchart_material" style="height: 300px;"></div>
                </div>
                <!-- /.card-body-->
            </div>
        </div>
    </div>
@stop