@extends('admin.layout')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Programas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
              <li class="breadcrumb-item ">Programa</li>
              <li class="breadcrumb-item active">Crear</li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card mr-4 ml-4">
    <div class="card-header">
    <h3 class="card-title">Registrar un Estudiante</h3>
    </div>
    <!-- /.card-header -->
<form method="POST" action="{{ route('admin.user.store') }}">
{{ csrf_field() }}
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Código:</label>
            <input type="number" class="form-control" name="id" placeholder="Ingrese un Código">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Nombre:</label>
            <input type="text" class="form-control" name="Name" placeholder="Ingrese un Nombre">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Primer Apellido:</label>
            <input type="text" class="form-control" name="Priname" placeholder="Ingrese su Primer Apellido">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Segundo Apellido:</label>
            <input type="text" class="form-control" name="Secname" placeholder="Ingrese su Segundo Apellido">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Correo Electronico:</label>
            <input type="email" class="form-control" name="Email" placeholder="Ingrese un Correo Electronico">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Contraseña:</label>
            <input type="password" class="form-control" name="Password" placeholder="Ingrese una Contraseña">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Nacionalidad:</label>
            <input type="text" class="form-control" name="Nacionality" placeholder="Ingrese una Nacionalidad">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Dirección:</label>
            <input type="text" class="form-control" name="Address" placeholder="Ingrese una Dirección">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Telefono:</label>
            <input type="text" class="form-control" name="Phone" placeholder="Ingrese un Telefono">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Ciudad de Residencia:</label>
            <input type="text" class="form-control" name="Cresidential" placeholder="Ingrese su ciudad de Residencia">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Ciudad de Origen:</label>
            <input type="text" class="form-control" name="Corigin" placeholder="Ingrese su ciudad de Origen">
        </div>       

        <div class="form-group">
            <label>Programa</label>
            <select name="codes[]" class="select2" multiple="multiple" data-placeholder="Selecciona las carreras" style="width: 100%;">
            @foreach($programas as $programa)
                <option value="{{ $programa->id }} " >{{ $programa->name_program }}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary float-right">Guardar</button>
    </div>
</form>
</div>

    


@stop