@extends('admin.layout')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Programas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
              <li class="breadcrumb-item ">Programa</li>
              <li class="breadcrumb-item active">Crear</li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card mr-4 ml-4">
    <div class="card-header">
    <h3 class="card-title">Crear un Programa</h3>
    </div>
    <!-- /.card-header -->
<form method="POST" action="{{ route('admin.programa.store') }}" enctype="multipart/form-data" files="true" >
{{ csrf_field() }} {{ method_field('PUT') }}
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Código:</label>
            <input type="number" class="form-control" name="Code" placeholder="Ingrese un Código">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Nombre:</label>
            <input type="text" class="form-control" name="Name" placeholder="Ingrese un Nombre">
        </div>

        <div class="form-group">
              <label for="exampleInputFile">Imagen:</label>
                <div class="col-md-12 custom-file margin-bottom">
                  <input class="custom-file-input" type="file" name="urlimg" required>
                  <label class="custom-file-label" for="exampleInputFile">Selecciona una imagen</label>
                </div>
            </div>

        <div class="form-group">
            <label>Facultad</label>
            <select class="form-control select2" name="Facultad" style="width: 100%;">
            @foreach($facultades as $facultad)
                <option value="{{ $facultad->id }} " >{{ $facultad->name_faculty }}</option>
            @endforeach
            </select>
        </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary float-right">Guardar</button>
    </div>
</form>
</div>

@stop