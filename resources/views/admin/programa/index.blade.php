@extends('admin.layout')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Programa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Programa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
@stop

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Listado de Programas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Facultad</th>
                  </tr>
                  </thead>
                  <tbody>

                  @foreach($programas as $programa)
                  <tr>
                    <td>{{ $programa->id }}</td>
                    <td>{{ $programa->name_program }}</td>
                    <td>{{ $programa->faculty->name_faculty }}</td>
                  </tr>
                  @endforeach


                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Facultad</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
@stop