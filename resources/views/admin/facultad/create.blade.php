@extends('admin.layout')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Facultad</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
              <li class="breadcrumb-item ">Facultad</li>
              <li class="breadcrumb-item active">Crear</li>

            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
@stop

@section('content')

<div class="card mr-4 ml-4">
    <div class="card-header">
    <h3 class="card-title">Crear una Facultad</h3>
    </div>
    <!-- /.card-header -->
<form method="POST" action="{{ route('admin.facultad.store') }}">
{{ csrf_field() }}
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Código:</label>
            <input type="number" class="form-control" name="Code" placeholder="Ingrese un Código">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Nombre:</label>
            <input type="text" class="form-control" name="Name" placeholder="Ingrese un Nombre">
        </div>

        <div class="form-group">
            <label>Campus</label>
            <select class="form-control select2" name="Campus" style="width: 100%;">
            @foreach ($campus as $campo)
                <option value="{{ $campo->id }} " >{{ $campo->name_campus }}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary float-right">Guardar</button>
    </div>
</form>
</div>

@stop