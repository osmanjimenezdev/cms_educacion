<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

    <li class="nav-item">
      <a href="{{ route('dashboard') }}" class="nav-link {{ request()->is('admin') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>
          Inicio
        </p>
      </a>
    </li>

    <li class="nav-item has-treeview ">
      <a href="#" class="nav-link {{ request()->is('admin/campus') ? 'active' : '' }}">
        <i class="nav-icon fas fa-map-marker-alt"></i>
        <p>
          Campus
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('admin.campus.create') }}" class="nav-link {{ request()->is('admin/campus/create') ? 'active' : '' }}">
            <i class="nav-icon fas fa-map-marked-alt"></i>
            <p>
              Agregar
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.campus.index') }}" class="nav-link {{ request()->is('admin/campus') ? 'active' : '' }}">
            <i class="nav-icon fas fa-map"></i>
            <p>
              Ver
            </p>
          </a>
        </li>
      </ul>
    </li>

    <li class="nav-item has-treeview ">
      <a href="#" class="nav-link {{ request()->is('admin/categorys') ? 'active' : '' }}">
        <i class="nav-icon fas fa-boxes"></i>
        <p>
          Facultades
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('admin.facultad.create') }}" class="nav-link {{ request()->is('admin/create') ? 'active' : '' }}">
            <i class="nav-icon fas fa-box"></i>
            <p>
              Agregar
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.facultad.index') }}" class="nav-link {{ request()->is('admin/categorys') ? 'active' : '' }}">
            <i class="nav-icon fas fa-box-open"></i>
            <p>
              Ver
            </p>
          </a>
        </li>
      </ul>
    </li>

    <li class="nav-item has-treeview ">
      <a href="#" class="nav-link {{ request()->is('admin/tags') ? 'active' : '' }}">
        <i class="nav-icon fas fa-table-tennis"></i>
        <p>
          Programas
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('admin.programa.create') }}" class="nav-link {{ request()->is('admin/create') ? 'active' : '' }}">
            <i class="nav-icon fas fa-baseball-ball"></i>
            <p>
              Agregar
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.programa.index') }}" class="nav-link {{ request()->is('admin/tags') ? 'active' : '' }}">
            <i class="nav-icon fas fa-passport"></i>
            <p>
              Ver
            </p>
          </a>
        </li>
      </ul>
    </li>

    <li class="nav-item has-treeview ">
      <a href="#" class="nav-link {{ request()->is('admin/users') ? 'active' : '' }}">
        <i class="nav-icon fas fa-user-friends"></i>
        <p>
          Estudiantes
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('admin.user.create') }}" class="nav-link {{ request()->is('admin/create') ? 'active' : '' }}">
            <i class="nav-icon fas fa-user"></i>
            <p>
              Agregar
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.user.index') }}" class="nav-link {{ request()->is('admin/users') ? 'active' : '' }}">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Ver
            </p>
          </a>
        </li>
      </ul>
    </li>
  </ul>
</nav>