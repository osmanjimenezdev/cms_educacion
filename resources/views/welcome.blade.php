@extends('layout')


@section('content')
      <!-- PROGRAMS -->

      <div class="container">
        <div class="row row-cols-1 row-cols-md-4 g-4 text-center">
            @foreach($programas as $programa)
            <div class="col">
              <div class="card h-100">
                <img src="/images/{{ $programa->urlimg }}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{ $programa->name_program }}</h5>
                </div>
                <div class="card-footer">
                  <small class="text-muted">{{ $programa->faculty->name_faculty }}</small>
              </div>
              </div>
            </div>
           @endforeach
          </div>
      </div>

      @stop

